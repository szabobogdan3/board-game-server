const PlayerRegistration = require("./player-registration");

function playerRegistration(game, min, max) {
  return new PlayerRegistration(...arguments);
}

module.exports = {
  playerRegistration
};