
class ReadyToStart {
  type = "ready-to-start"

  constructor(game, playerName) {
    this.game = game;
    this.by = playerName;
  }

  handle(params) {
    this.game.players
      .filter(a => a.name == this.by)
      .forEach(player => {
        player.ready = params.ready;
      });
  }

  toJSON() {
    return {
      by: this.by,
      type: this.type
    }
  }
}

module.exports = ReadyToStart;
