
class BeginGame {
  type = "begin-game"
  by = "*"

  constructor(game) {
    this.game = game;
  }

  handle() {
    if(!this.game.beginGame) {
      throw new Error("The game must have a beginGame() method.");
    }

    this.game.beginGame();
  }

  toJSON() {
    return {
      by: this.by,
      type: this.type
    }
  }
}

module.exports = BeginGame;
