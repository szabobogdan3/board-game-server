const AddPlayer = require('./add-player');
const ReadyToStart = require('./ready-to-start');
const BeginGame = require('./begin-game');

function addPlayer(game) {
  return new AddPlayer(game);
}

function readyToStart(game, playerName) {
  return new ReadyToStart(game, playerName);
}

function beginGame(game) {
  return new BeginGame(game);
}

module.exports = {
  addPlayer,
  readyToStart,
  beginGame
};