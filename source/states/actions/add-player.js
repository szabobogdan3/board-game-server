
class AddPlayer {
  type = "add-player"
  by = "*"

  constructor(game) {
    this.game = game;
  }

  handle(params) {
    if(!Array.isArray(this.game.players)) {
      this.game.players = [];
    }

    if(!params.name) {
      throw new Error("You must provide a 'name' for the new player.");
    }

    if(this.game.players.filter(a => a.name == params.name).length > 0) {
      throw new Error("The player is already registered.");
    }

    this.game.players.push(params);
  }

  toJSON() {
    return {
      by: this.by,
      type: this.type
    }
  }
}

module.exports = AddPlayer;
