const actions = require("./actions");

/// A game state that allows players to register
class PlayerRegistrationState {

  name = "player-registration"

  /// Returns the `add-player` action if the max number of players is not achieved
  /// and if the min number of players is achieved the ready-to-start action is returned
  constructor(game, min = 2, max = 4) {
    this.game = game;
    this.min = min;
    this.max = max;
  }

  /// Get the list of the available actions
  actions() {
    if(this.min <= 0) {
      throw new Error("The player registration state must have at least 1 player.");
    }

    if(this.min > this.max) {
      throw new Error("The player registration state must have the 'max' numbers of players >= than the 'min' one.");
    }

    const list = [ ];

    if(this.game.players.length < this.max) {
      list.push(actions.addPlayer(this.game));
    }

    this.game.players
      .filter(a => !a.ready)
      .map(a => actions.readyToStart(this.game, a.name))
      .forEach(element => {
        list.push(element);
      });

    const readyPlayerCount = this.game.players.filter(a => a.ready).length;

    if(readyPlayerCount >= this.min) {
      list.push(actions.beginGame(this.game))
    }

    return list;
  }
}


module.exports = PlayerRegistrationState;