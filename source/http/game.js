const Boom = require('@hapi/boom');
const Joi = require('@hapi/joi');

const GameSchema = Joi.object({
  game: Joi.object({
    type: Joi.string()
      .min(3)
      .max(30)
      .required()
  })
});

async function setup(server, repository) {
  server.route({
    method: 'POST',
    path: '/games',
    async handler(request) {
      try {
        await GameSchema.validateAsync(request.payload);

        const game = await repository.create(request.payload.game.type);
        return { game: game.toJSON() };
      } catch (err) {
        return Boom.badRequest(err);
      }
    }
  });

  server.route({
    method: 'POST',
    path: '/games/{gameId}/{action}',
    async handler(request) {
      try {
        const game = await repository.get(request.params.gameId);
        game.handle(request.params.action, request.payload.by, request.payload.params );

        return game.toJSON();
      } catch (err) {
        return Boom.badRequest(err);
      }
    }
  });
}

module.exports.setup = setup;