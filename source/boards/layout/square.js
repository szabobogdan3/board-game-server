const Layout = require("./layout");

class SquareLayout extends Layout {
  name = "square"

  getHorizontalNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x: x - 1, y: 0, z }, { x: x + 1, y: 0, z }];
  }

  getTopNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x: x - 1, y: y - 1, z }, { x, y: y - 1, z }, { x: x + 1, y: y - 1, z }];
  }

  getBottomNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x: x - 1, y: y + 1, z }, { x, y: y + 1, z }, { x: x + 1, y: y + 1, z }];
  }

  getRingStart(point, radius) {
    return { x: point.x - radius, y: point.y + radius, z: 0 };
  }

  directions() {
    return [
      { x: 1, y: 0, z: 0 },
      { x: 0, y: -1, z: 0 },
      { x: -1, y: 0, z: 0 },
      { x: 0, y: 1, z: 0 }
    ];
  }

  /// generate a ring of coordinates with a given radius
  ring(point, radius) {
    if (radius < 0) {
      return [];
    }

    if (radius == 0) {
      return [point];
    }

    let current = this.getRingStart(point, radius);
    const list = [];

    this.directions().forEach(direction => {
      for (let i = 0; i < radius * 2; i++) {
        list.push(current);
        current = {
          x: current.x + direction.x,
          y: current.y + direction.y,
          z: current.z
        };
      }
    });

    return list;
  }
}

module.exports = SquareLayout;
