

class Layout {

  getPoint() {
    let x;
    let y;
    let z;

    if (arguments.length == 1) {
      x = arguments[0].x;
      y = arguments[0].y;
      z = arguments[0].z;
    }

    if (arguments.length == 2) {
      x = arguments[0];
      y = arguments[1];
      z = 0;
    }

    if (arguments.length == 3) {
      x = arguments[0];
      y = arguments[1];
      z = arguments[2];
    }

    return { x, y, z };
  }

  getPoints() {
    let x1, x2;
    let y1, y2;
    let z1, z2;

    if (arguments.length == 2) {
      x1 = arguments[0].x;
      y1 = arguments[0].y;
      z1 = arguments[0].z;

      x2 = arguments[1].x;
      y2 = arguments[1].y;
      z2 = arguments[1].z;
    }

    if (arguments.length == 6) {
      x1 = arguments[0];
      y1 = arguments[1];
      z1 = arguments[2];

      x2 = arguments[3];
      y2 = arguments[4];
      z2 = arguments[5];
    }

    return [{ x: x1, y: y1, z: z1 }, { x: x2, y: y2, z: z2 }];
  }

  getNeighbors() {
    const point = this.getPoint(...arguments);
    return this.ring(point, 1);
  }

  getBorderNeighbors() {
    const [point, origin] = this.getPoints(...arguments);
    const distance = this.distance(...arguments);

    let list = this.getNeighbors(point).filter(a => this.distance(a, origin) > distance);

    return list;
  }

  distance() {
    const [point1, point2] = this.getPoints(...arguments);

    return (Math.abs(point1.x - point2.x) + Math.abs(point1.y - point2.y) + Math.abs(point1.z - point2.z)) / 2;
  }

  areEqual(point1, point2) {
    return point1.x == point2.x && point1.y == point2.y && point1.z == point2.z;
  }

  contains(list, point) {
    return list.filter(a => this.areEqual(a, point)).length > 0;
  }

  /// generate a list of coordinates that fit in a circle of a given radius
  circle(point, radius) {
    let list = [];

    for (let i = 0; i <= radius; i++) {
      list = list.concat(this.ring(point, i));
    }

    return list;
  }
}


module.exports = Layout;