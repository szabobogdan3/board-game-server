const Layout = require("./layout");

class HexagonalLayout extends Layout {
  name = "hexagonal"

  getHorizontalNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x: x - 1, y: y + 1, z }, { x: x + 1, y: y - 1, z }];
  }

  getTopNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x, y: y + 1, z: z - 1 }, { x: x + 1, y, z: z - 1 }];
  }

  getBottomNeighbors() {
    const { x, y, z } = this.getPoint(...arguments);

    return [{ x: x - 1, y, z: z + 1 }, { x, y: y - 1, z: z + 1 }]
  }

  directions() {
    return [
      { x: 1, y: -1, z: 0 },
      { x: 1, y: 0, z: -1 },
      { x: 0, y: 1, z: -1 },
      { x: -1, y: 1, z: 0 },
      { x: -1, y: 0, z: +1 },
      { x: 0, y: -1, z: +1 }
    ];
  }

  getRingStart(point, radius) {
    return { x: point.x - radius, y: point.y , z: point.z + radius };
  }

  /// generate a ring of coordinates with a given radius
  ring(point, radius) {
    if (radius < 0) {
      return [];
    }

    if (radius == 0) {
      return [point];
    }

    let current = this.getRingStart(point, radius);
    const list = [];

    this.directions().forEach(direction => {
      for (let i = 0; i < radius; i++) {
        list.push(current);
        current = {
          x: current.x + direction.x,
          y: current.y + direction.y,
          z: current.z + direction.z
        };
      }
    });

    return list;
  }
}

module.exports = HexagonalLayout;
