

class Board {
  role = "primary";

  constructor(layout) {
    this.layout = layout;
    this.points = [];
    this.tiles = [];
    this.tokens = [];
  }

  /// Calls the layout `operation` method using the remaining arguments
  /// and uses the result as the point list
  generate(operation) {
    this.points = this.layout[operation].call(this.layout, ...[...arguments].slice(1));
    this.items = [];
  }

  /// Add the tile to the next available point
  addTile(tile) {
    if(!this.points || this.points.length == 0) {
      throw new Error("The board has no defined points.");
    }

    if(this.points.length == this.tiles.filter(a => !!a).length) {
      throw new Error("All board tiles are already set.");
    }

    if(this.points.length > this.tiles.length) {
      return this.tiles.push(tile);
    }

    for(let index = 0; index<this.tiles.length; index++) {
      if(!this.tiles[index]) {
        this.tiles[index] = tile;
        return true;
      }
    }
  }

  /// Add the tile to the the specified point
  addTileAt(point, tile) {
    if(!this.points || this.points.length == 0) {
      throw new Error("The board has no defined points.");
    }

    let added;
    this.points.forEach((p, index) => {
      if(this.layout.areEqual(point, p)) {
        this.tiles[index] = tile;
        added = true;
      }
    });

    if(!added) {
      throw new Error("The point is not in the point list.");
    }
  }

  setToken(point, token) {
    if(!this.points || this.points.length == 0) {
      throw new Error("The board has no defined points.");
    }

    this.removeToken(token);

    let added;
    this.points.forEach((p, index) => {
      if(this.layout.areEqual(point, p)) {
        this.tokens[index].push(token);
        added = true;
      }
    });

    if(!added) {
      throw new Error("The point is not in the point list.");
    }
  }

  removeToken(token) {
    const id = token.id || token;

    this.points.forEach((p, index) => {
      if(!Array.isArray(this.tokens[index])) {
        this.tokens[index] = [];
        return;
      }

      this.tokens[index] = this.tokens[index].filter(a => a.id = id);
    });
  }

  /// Converts the board to a plain json to be sent to clients or stored to the database
  toJSON() {
    return {
      role: this.role,
      layout: this.layout.name,
      points: this.points.map(a => a.toJSON ? a.toJSON() : a),
      tiles: this.tiles.map(a => a.toJSON ? a.toJSON() : a),
      tokens: this.tokens.map(a => a.map(b => b.toJSON ? b.toJSON() : b)),
    };
  }
}

module.exports = Board;