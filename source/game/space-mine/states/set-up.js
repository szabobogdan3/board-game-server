const Board = require("../../../boards/board");
const Deck = require("../../../decks/deck")
const HexagonalLayout = require("../../../boards/layout/hexagonal");

/// The space mine game setup state
class SetUp {

  name = "set-up"
  ironIndex = 0
  stoneIndex = 0
  gasIndex = 0
  iceIndex = 0
  rareIndex = 0
  voidIndex = 0

  /// It creates the boards and allows the players to set their start
  /// position
  constructor(game) {
    this.game = game;

    if (!game.boards) {
      this.setupBoards();
    }
  }

  setupBoards() {
    this.game.boards = [];
    const deck = new Deck();

    for(var i = 0; i<=3; i++) {
      deck.push(this.createIronCard());
      deck.push(this.createStoneCard());
      deck.push(this.createGasCard());
    }

    for(i = 0; i<=4; i++) {
      deck.push(this.createIceCard());
    }

    deck.push(this.createRareCard());
    deck.push(this.createVoidCard());
    deck.shuffle();

    const board = new Board(new HexagonalLayout());
    board.generate("circle", { x: 0, y: 0, z: 0 }, 2);

    deck.forEach(card => {
      board.addTile(card);
    });

    this.game.boards.push(board);
  }

  createIronCard() {
    const id = "iron-" + this.ironIndex;
    this.ironIndex++;

    return {
      type: "iron",
      resource: "iron",
      id
    }
  }

  createStoneCard() {
    const id = "stone-" + this.stoneIndex;
    this.stoneIndex++;

    return {
      type: "stone",
      resource: "silicate",
      id
    }
  }

  createGasCard() {
    const id = "gas-" + this.gasIndex;
    this.gasIndex++;

    return {
      type: "gas",
      resource: "hydrogen",
      id
    }
  }

  createIceCard() {
    const id = "ice-" + this.iceIndex;
    this.iceIndex++;

    return {
      type: "ice",
      resource: "water",
      id
    }
  }

  createRareCard() {
    const id = "rare-" + this.rareIndex;
    this.rareIndex++;

    return {
      type: "rare",
      resource: "rare",
      id
    }
  }

  createVoidCard() {
    const id = "void-" + this.voidIndex;
    this.voidIndex++;

    return {
      type: "void",
      resource: "void",
      id
    }
  }

  /// Get the list of the available actions
  actions() {
    const list = [];

    return list;
  }
}

module.exports = SetUp;
