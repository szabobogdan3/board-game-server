const SetUp = require("./set-up");

function setUp(game) {
  return new SetUp(...arguments);
}

module.exports = {
  setUp
};