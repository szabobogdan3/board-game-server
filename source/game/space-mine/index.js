const States = require("../../states");
const SpaceMineStates = require("./states");

class SpaceMine {
  players = [];
  state = null;

  static getTypeName() {
    return "space-mine";
  }

  static fromJson(data) {
    return new SpaceMine(data);
  }

  constructor(data) {
    this.data = data;

    if(data._id.toString) {
      this.data._id = data._id.toString();
    }

    if(!this.data || !this.data.state) {
      this.state = States.playerRegistration(this);
    }
  }

  actions() {
    return this.state.actions();
  }

  beginGame() {
    this.state = SpaceMineStates.setUp(this);
  }

  handle(type, playerName, params) {
    const actions = this.actions().filter(a => a.by == playerName && a.type == type);
    actions[0].handle(params);
  }

  toJSON() {
    const json = {
      _id: this.data._id,
      type: SpaceMine.getTypeName(),
      state: this.state.name,
      actions: this.actions().map(a => a.toJSON ? a.toJSON() : a)
    };

    if(this.boards) {
      json.boards = this.boards.map(a => a.toJSON())
    }

    return json;
  }
}

module.exports = SpaceMine;