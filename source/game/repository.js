class GameRepository {
  games = {};
  gamesCollection = null;

  constructor(gamesCollection) {
    this.gamesCollection = gamesCollection;
  }

  add(Type) {
    this.games[Type.getTypeName()] = Type;
  }

  async create(gameType) {
    if(!this.games[gameType]) {
      throw new Error(`The '${gameType}' does not exist.`)
    }

    const newGame = {
      type: gameType
    };

    const result = await this.gamesCollection.insert(newGame);
    newGame["_id"] = result.insertedIds[0].toString();

    return this.games[gameType].fromJson(newGame);
  }

  async get(_id) {
    const gameData = await this.gamesCollection.findOne({ _id });
    const type = gameData["type"];

    return this.games[type].fromJson(gameData);
  }
}

module.exports = GameRepository;