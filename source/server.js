const Hapi = require('@hapi/hapi');
const Game = require('./http/game');

exports.init = async function(db, gameRepo, log) {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  });

  await Game.setup(server, gameRepo);

  await server.start();

  if(log) {
    console.log('Server running on %s', server.info.uri);
  }

  return server;
};
