class Deck extends Array {
  constructor(items) {
    super();

    if (!items || items.length == 0) {
      return;
    }

    for (let i = 0; i < items.length; i++) {
      this.push(items[i]);
    }
  }

  shuffle() {
    for (let i = this.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [this[i], this[j]] = [this[j], this[i]];
    }
  }
}


module.exports = Deck;