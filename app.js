const Server = require('./source/server');

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

Server.init();