require('chai').should();

const SetUpState = require('../../../../source/game/space-mine/states/set-up');

describe("The space mine setup", function () {
  it("should initialize a round hexagonal map", function () {
    const game = {};
    const state = new SetUpState(game);

    game.boards.length.should.equal(1);
    const board = game.boards[0].toJSON();

    board.layout.should.equal("hexagonal");
    board.role.should.equal("primary");
    board.points.length.should.equal(19);
    board.tiles.length.should.equal(19);
  });
});