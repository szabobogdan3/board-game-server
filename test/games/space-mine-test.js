require('chai').should();

const SpaceMine = require('../../source/game/space-mine');

describe("The space mine game", function () {
  it("should have the 'space-mine' type", function () {
    SpaceMine.getTypeName().should.equal("space-mine");
  });

  describe("with a new instance", function () {
    let game;

    beforeEach(function () {
      game = new SpaceMine({ _id: 1 });
    });

    it("should wait for the first player to register", function () {
      game.toJSON().should.deep.equal({
        _id: "1",
        type: "space-mine",
        state: "player-registration",
        actions: [{ type: "add-player", by: "*" }]
      });
    });

    it("should add a new player when the add player action is handled", function () {
      game.handle("add-player", "*", { name: "player1" });
      game.players.length.should.equal(1);
      game.players.should.deep.equal([{
        name: "player1"
      }]);
    });

    it("should allow adding a new player and setting the first one ready after an 'add-player' action was handled", function () {
      game.handle("add-player", "*", { name: "player1" });
      game.actions().map(a => a.toJSON()).should.deep.equal([{ type: "add-player", by: "*" }, { by: "player1", type: "ready-to-start" }]);
    });

    it("should mark a player ready when the ready to start action is handled", function () {
      game.players = [{ name: "player1" }];

      game.handle("ready-to-start", "player1", { ready: true });
      game.players.length.should.equal(1);
      game.players.should.deep.equal([{
        name: "player1",
        ready: true
      }]);
    });

    it("should allow the game to begin when there are 4 ready players", function () {
      game.players = [
        { name: "player1", ready: true },
        { name: "player2", ready: true },
        { name: "player3", ready: true },
        { name: "player4", ready: true }];

      game.actions().map(a => a.toJSON()).should.deep.equal([{ by: '*', type: 'begin-game' }]);
    });

    it("should be in the set-up state when the begin-game is handled", function () {
      game.players = [
        { name: "player1", ready: true },
        { name: "player2", ready: true },
        { name: "player3", ready: true },
        { name: "player4", ready: true }];

      game.handle("begin-game", "*", { });

      game.toJSON().state.should.equal("set-up");
    });
  });
});
