require('chai').should();

const Deck = require('../../source/decks/deck');

describe("The deck", function () {
  let deck;

  beforeEach(function () {
    deck = new Deck();
  });

  it("should push and pop an item", function () {
    deck.push(1);
    deck.pop().should.equal(1);
  });

  it("should be able to access the first element", function () {
    deck.push(1);
    deck[0].should.equal(1);
  });

  it("should be able to initialize it with a list of elements", function () {
    deck = new Deck([11, 22, 33]);
    deck.should.deep.equal([11, 22, 33]);
  });

  it("should be able to shuffle the items", function () {
    deck = new Deck([11, 22, 33, 44, 55, 66, 77, 88, 99, 111, 222, 333]);

    deck.shuffle();
    deck.length.should.equal(12);
    deck.should.not.deep.equal([11, 22, 33, 44, 55, 66, 77, 88, 99, 111, 222, 333]);
  });

});