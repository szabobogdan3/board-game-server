const should = require('chai').should();

const SquareLayout = require('../../../source/boards/layout/square');

describe("The Square Layout", function () {
  let layout;

  beforeEach(function () {
    layout = new SquareLayout();
  });

  describe("generating the neighbors", function () {
    it('should return a list of 6 neighbors for the 0,0 coordinate', function () {
      layout.getNeighbors(0, 0).should.deep.include.members([
        { x: -1, y: -1, z: 0 }, { x: 0, y: -1, z: 0 }, { x: 1, y: -1, z: 0 },
        { x: -1, y: 0, z: 0 }, { x: 1, y: 0, z: 0 },
        { x: -1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }, { x: 1, y: 1, z: 0 }
      ]);

      layout.getNeighbors({ x: 0, y: 0 }).should.deep.include.members([
        { x: -1, y: -1, z: 0 }, { x: 0, y: -1, z: 0 }, { x: 1, y: -1, z: 0 },
        { x: -1, y: 0, z: 0 }, { x: 1, y: 0, z: 0 },
        { x: -1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }, { x: 1, y: 1, z: 0 }
      ]);
    });

    it('should return two horizontal elements for for the 0,0 coordinate', function () {
      const horizontal = [{ x: -1, y: 0, z: 0 }, { x: 1, y: 0, z: 0 }];

      layout.getHorizontalNeighbors(0, 0, 0).should.deep.include.members(horizontal);
      layout.getHorizontalNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(horizontal);
    });

    it('should return two top neighbors for the 0,0 coordinate', function () {
      const top = [{ x: -1, y: -1, z: 0 }, { x: 0, y: -1, z: 0 }, { x: 1, y: -1, z: 0 }];

      layout.getTopNeighbors(0, 0, 0).should.deep.include.members(top);
      layout.getTopNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(top);
    });

    it('should return two bottom neighbors for the 0,0 coordinate', function () {
      const bottom = [{ x: -1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }, { x: 1, y: 1, z: 0 }];

      layout.getBottomNeighbors(0, 0, 0).should.deep.include.members(bottom);
      layout.getBottomNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(bottom);
    });

    it('should return the neighbors that are at the exterior relative to a point', function () {
      const border = [{ x: 3, y: -2, z: 0 }, {x: 3, y: -3, z: 0}, {x: 2, y: -3, z: 0} ];

      layout.getBorderNeighbors(2, -2, 0, 0, 0, 0).should.deep.include.members(border);
    });
  });

  describe("getting the list of coordinates of a ring", function() {
    it("should return the point for the radius 0", function() {
      const point = { x: 0, y: 0, z: 0 };
      layout.ring(point, 0).should.deep.include.members([ point ]);
    });

    it("should return the point for the radius 1", function() {
      const point = { x: 0, y: 0, z: 0 };
      layout.ring(point, 1).filter(a => layout.areEqual(a, point)).length.should.equal(0);
    });

    it("should not find in a ring with radius 2 elements from the ring with radius 1", function() {
      const point = { x: 0, y: 0, z: 0 };
      const ring1 = layout.ring(point, 1);
      const ring2 = layout.ring(point, 2);

      ring2.filter(a => !layout.contains(ring1, a)).length.should.equal(ring2.length);
    });
  });

  describe("getting the list of coordinates of a circle table", function () {
    it("should return only the point when the radius is 0", function () {
      layout.circle({ x: 0, y: 0, z: 0 }, 0).should.deep.equal([{ x: 0, y: 0, z: 0 }]);
    });

    it("should return the point and it's neighbors when the radius is 1", function () {
      const list = [
        { x: 0, y: 0, z: 0 },

        { x: -1, y: -1, z: 0 }, { x: 0, y: -1, z: 0 }, { x: 1, y: -1, z: 0 },
        { x: -1, y: 0, z: 0 }, { x: 1, y: 0, z: 0 },
        { x: -1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }, { x: 1, y: 1, z: 0 },
      ];

      layout.circle({ x: 0, y: 0, z: 0 }, 1).should.deep.include.members(list);
    });

    it("should return the list of points from ring 0,1 and 2 when the radius is 2", function () {
      const list = [
        { x: 0, y: 0, z: 0 },

        { x: -1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }, { x: 1, y: 1, z: 0 },
        { x: 1, y: 0, z: 0 },
        { x: 1, y: -1, z: 0 }, { x: 0, y: -1, z: 0 }, { x: -1, y: -1, z: 0 },
        { x: -1, y: 0, z: 0 },

        { x: -2, y: 2, z: 0 }, { x: -1, y: 2, z: 0 }, { x: 0, y: 2, z: 0 }, { x: 1, y: 2, z: 0 }, { x: 2, y: 2, z: 0 },
        { x: 2, y: -1, z: 0 },
        { x: 2, y: 0, z: 0 },
        { x: 2, y: 1, z: 0 },
        { x: 2, y: -2, z: 0 }, { x: 1, y: -2, z: 0 },{ x: 0, y: -2, z: 0 }, { x: -1, y: -2, z: 0 }, { x: -2, y: -2, z: 0 },
        { x: -2, y: 1, z: 0 },
        { x: -2, y: 0, z: 0 },
        { x: -2, y: -1, z: 0 }
      ];

      layout.circle({ x: 0, y: 0, z: 0 }, 2).should.deep.include.members(list);
    });

    it("should have 16 new elements with radius 2 from radius 1", function () {
      const len = layout.circle({ x: 0, y: 0, z: 0 }, 1).length;
      (layout.circle({ x: 0, y: 0, z: 0 }, 2).length - len).should.equal(16);
    });

    it("should have 24 new elements with radius 3 from radius 2", function () {
      const len = layout.circle({ x: 0, y: 0, z: 0 }, 2).length;
      (layout.circle({ x: 0, y: 0, z: 0}, 3).length - len).should.equal(24);
    });
  });
});