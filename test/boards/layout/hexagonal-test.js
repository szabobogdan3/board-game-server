const should = require('chai').should();

const HexagonalLayout = require('../../../source/boards/layout/hexagonal');

describe("The Hexagonal Layout", function () {
  let layout;

  beforeEach(function () {
    layout = new HexagonalLayout();
  });

  describe("generating the neighbors", function () {
    it('should return a list of 6 neighbors for the 0,0 coordinate', function () {
      layout.getNeighbors(0, 0, 0).should.deep.include.members([
        { x: +1, y: -1, z: 0 }, { x: +1, y: 0, z: -1 }, { x: 0, y: +1, z: -1 },
        { x: -1, y: +1, z: 0 }, { x: -1, y: 0, z: +1 }, { x: 0, y: -1, z: +1 }
      ]);

      layout.getNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members([
        { x: +1, y: -1, z: 0 }, { x: +1, y: 0, z: -1 }, { x: 0, y: +1, z: -1 },
        { x: -1, y: +1, z: 0 }, { x: -1, y: 0, z: +1 }, { x: 0, y: -1, z: +1 }
      ]);
    });

    it('should return two horizontal elements for for the 0,0 coordinate', function () {
      const horizontal = [{ x: +1, y: -1, z: 0 }, { x: -1, y: +1, z: 0 }];

      layout.getHorizontalNeighbors(0, 0, 0).should.deep.include.members(horizontal);
      layout.getHorizontalNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(horizontal);
    });

    it('should return two top neighbors for the 0,0 coordinate', function () {
      const top = [{ x: +1, y: 0, z: -1 }, { x: 0, y: +1, z: -1 }];

      layout.getTopNeighbors(0, 0, 0).should.deep.include.members(top);
      layout.getTopNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(top);
    });

    it('should return two bottom neighbors for the 0,0 coordinate', function () {
      const bottom = [{ x: -1, y: 0, z: +1 }, { x: 0, y: -1, z: +1 }];

      layout.getBottomNeighbors(0, 0, 0).should.deep.include.members(bottom);
      layout.getBottomNeighbors({ x: 0, y: 0, z: 0 }).should.deep.include.members(bottom);
    });

    it('should return the neighbors that are at the exterior relative to a point', function () {
      const border = [{ x: 3, y: -3, z: 0 }, {x: 3, y: -2, z: -1}, {x: 2, y: -3, z: 1} ];

      layout.getBorderNeighbors(2, -2, 0, 0, 0, 0).should.deep.include.members(border);
    });
  });

  describe("the point equality", function () {
    it("should not find a point in it's neighbor list", function () {
      layout.getNeighbors({ x: 0, y: 0, z: 0 }).map(a => layout.areEqual({ x: 0, y: 0, z: 0 }, a)).should.deep.equal([false, false, false, false, false, false]);
    });

    it("a point should equal with itself", function () {
      layout.areEqual({ x: 0, y: 0, z: 0 }, { x: 0, y: 0, z: 0 }).should.equal(true);
    });
  });

  describe("getting the distance between tiles", function () {
    it("should have distance 0 for a point to itself", function () {
      layout.distance({ x: 0, y: 0, z: 0 }, { x: 0, y: 0, z: 0 }).should.equal(0);
    });

    it("should have distance 1 for all neighbors", function () {
      layout.getNeighbors(0, 0, 0).map(a => layout.distance(a, { x: 0, y: 0, z: 0 })).should.deep.equal([1, 1, 1, 1, 1, 1]);
    });

    it("should have distance 3 between -2,2,0 and origin", function () {
      layout.distance({ x: -2, y: 2, z: 0 }, { x: 0, y: 0, z: 0 }).should.equal(2);
    });

    it("should have only distance 3, 2 and 1 for neighbors of a neighbor", function () {
      layout.getNeighbors(2, -2, 0)
        .map(a => layout.distance(a, { x: 0, y: 0, z: 0 }))
        .should.deep.include.members([1, 3, 2, 3, 2, 3]);
    });

    it("should be a commutative operation", function () {
      const a = layout.getNeighbors(0, 0, 0).map(a => layout.distance(a, { x: 0, y: 0, z: 0 }));
      const b = layout.getNeighbors(0, 0, 0).map(a => layout.distance({ x: 0, y: 0, z: 0 }, a));

      a.should.deep.equal(b);
    });
  });

  describe("check if a list contains a point", function () {
    it("should return only the point when the radius is 0", function () {
      layout.contains([{ x: 0, y: 0, z: 0 }], { x: 0, y: 0, z: 0 }).should.equal(true);
    });

    it("should return only the point when the radius is 0", function () {
      layout.contains([{ x: 1, y: 1, z: 0 }], { x: 0, y: 0, z: 0 }).should.equal(false);
    });
  });

  describe("getting the list of coordinates of a circle table", function () {
    it("should return only the point when the radius is 0", function () {
      layout.circle({ x: 0, y: 0, z: 0 }, 0).should.deep.equal([{ x: 0, y: 0, z: 0 }]);
    });

    it("should return the point and it's neighbors when the radius is 1", function () {
      const list = [
        { x: 0, y: 0, z: 0 },
        { x: -1, y: 0, z: 1 },
        { x: 0, y: -1, z: 1 },
        { x: 1, y: -1, z: 0 },
        { x: 1, y: 0, z: -1 },
        { x: 0, y: 1, z: -1 },
        { x: -1, y: 1, z: 0 }
      ];

      layout.circle({ x: 0, y: 0, z: 0 }, 1).should.deep.include.members(list);
    });

    it("should return a list of 19 points when the radius is 2", function () {
      layout.circle({ x: 0, y: 0, z: 0 }, 2).length.should.equal(19);
    });

    it("should have 12 new elements with radius 2 from radius 1", function () {
      const len = layout.circle({ x: 0, y: 0, z: 0 }, 1).length;
      (layout.circle({ x: 0, y: 0, z: 0 }, 2).length - len).should.equal(12);
    });

    it("should have 18 new elements with radius 3 from radius 2", function () {
      const len = layout.circle({ x: 0, y: 0, z: 0 }, 2).length;
      (layout.circle({ x: 0, y: 0, z: 0}, 3).length - len).should.equal(18);
    });
  });
});
