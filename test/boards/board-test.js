const should = require('chai').should();

const Board = require('../../source/boards/board');

describe("The board", function () {
  let board;

  describe("positions", function () {
    it('should be generated using a layout operation', function () {
      let params = [];
      const twoPoints = [{ x: 1, y: 1 }, { x: 0, y: 0 }];

      board = new Board({
        circle: function () {
          params = [...arguments];

          return twoPoints;
        }
      });

      board.generate("circle", 1, 2, 3);
      params.should.deep.equal([1, 2, 3]);
      board.points.should.deep.equal(twoPoints);
    });
  });

  describe("serialization", function () {
    it("should be successfully for an empty board", function () {
      board = new Board({ name: "test" });

      board.toJSON().should.deep.equal({
        role: "primary",
        layout: "test",
        points: [],
        tiles: [],
        tokens: []
      });
    });

    it("should be successfully for an board with one item in every list", function () {
      board = new Board({ name: "test" });
      board.points = [{ x: 0, y: 0 }];
      board.tiles = [{ id: "tile1" }];
      board.tokens = [[{ id: "token1" }]];

      board.toJSON().should.deep.equal({
        role: "primary",
        layout: "test",
        points: [{ x: 0, y: 0 }],
        tiles: [{ id: "tile1" }],
        tokens: [[{ id: "token1" }]]
      });
    });

    it("should call the toJSON method when an item has it", function () {
      board = new Board({ name: "test" });
      board.points = [{ toJSON() { return { x: 0, y: 0 } } }];
      board.tiles = [{ toJSON() { return { id: "tile1" } } }];
      board.tokens = [[{ toJSON() { return { id: "token1" } } }]];

      board.toJSON().should.deep.equal({
        role: "primary",
        layout: "test",
        points: [{ x: 0, y: 0 }],
        tiles: [{ id: "tile1" }],
        tokens: [[{ id: "token1" }]]
      });
    });
  });

  describe("tiles", function () {
    it("should throw an error when adding a tile to a board with no points", function () {
      board = new Board({ name: "test" });

      (function () {
        board.addTile({});
      }).should.throw('The board has no defined points.');
    });

    it("should throw an error when adding a tile at a point to board with no points", function () {
      board = new Board({ name: "test" });

      (function () {
        board.addTileAt({}, {});
      }).should.throw('The board has no defined points.');
    });

    it("should add a tile when there is a defined point", function () {
      board = new Board({ name: "test" });
      board.points = [{ x: 0, y: 0 }]

      board.addTile({ id: "tile" });

      board.tiles.should.deep.equal([{ id: "tile" }]);
    });

    it("should throw while adding a second tile for a map with one point", function () {
      board = new Board({ name: "test" });
      board.points = [{ x: 0, y: 0 }]

      board.addTile({ id: "tile" });

      (function () {
        board.addTile({});
      }).should.throw('All board tiles are already set.');
    });

    it("should be able to add a tile to an existing point", function () {
      board = new Board({
        name: "test",
        areEqual(a, b) { return a.x == b.x && a.y == b.y; } });
      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }]

      board.addTileAt({ x: 1, y: 1 }, { id: "tile" });
      board.tiles.should.deep.equal([undefined, { id: "tile" }]);
    });

    it("should be able to add a tile to the first point if the second is taken", function () {
      board = new Board({
        name: "test",
        areEqual(a, b) { return a.x == b.x && a.y == b.y; } });
      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }]

      board.addTileAt({ x: 1, y: 1 }, { id: "tile2" });
      board.addTile({ id: "tile1" });
      board.tiles.should.deep.equal([{ id: "tile1" }, { id: "tile2" }]);
    });

    it("should throw an error when adding a tile at a missing point", function () {
      board = new Board({
        name: "test",
        areEqual(a, b) { return a.x == b.x && a.y == b.y; } });

      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }]

      try {
        board.addTileAt({ x: 10, y: 10 }, { id: "tile2" });
      } catch(err) {
        error = err
      }

      error.message.should.equal('The point is not in the point list.');
    });
  });

  describe("tokens", function() {
    it("throws an error on adding a token to a board with no points", function() {
      board = new Board({ name: "test" });

      (function () {
        board.setToken({ x: 10, y: 10 }, { id: "token" });
      }).should.throw('The board has no defined points.');
    });

    it("adds a token to a point if the point exists", function() {
      board = new Board({ name: "test", areEqual(a, b) { return a.x == b.x && a.y == b.y; }});
      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }];

      board.setToken({ x: 1, y: 1 }, { id: "token" });
      board.tokens.should.deep.equal([[],[{ id: "token" }]]);
    });

    it("removes a token by the token object", function() {
      board = new Board({ name: "test", areEqual(a, b) { return a.x == b.x && a.y == b.y; }});
      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }];
      board.tokens = [[],[{ id: "token" }]];

      board.removeToken({ id: "token" });
    });

    it("removes a token by id", function() {
      board = new Board({ name: "test", areEqual(a, b) { return a.x == b.x && a.y == b.y; }});
      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }];
      board.tokens = [[],[{ id: "token" }]];

      board.removeToken("token");
    });

    it("should throw an error on add in a token to a missing point", function () {
      board = new Board({
        name: "test",
        areEqual(a, b) { return a.x == b.x && a.y == b.y; } });

      board.points = [{ x: 0, y: 0}, { x: 1, y: 1 }]

      try {
        board.setToken({ x: 1, y: 1 }, { id: "token" });
      } catch(err) {
        error = err
      }

      error.message.should.equal('The point is not in the point list.');
    });
  });
});
