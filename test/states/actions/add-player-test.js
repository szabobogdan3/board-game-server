require('chai').should();

const AddPlayerAction = require('../../../source/states/actions/add-player');

describe("The add player action", function () {
  it("should add a player list if it does not exist", function() {
    const game = {}

    const action = new AddPlayerAction(game);
    action.handle({name: "test"});

    game.should.deep.equal({
      players: [{
        name: "test"
      }]
    });
  });

  it("should throw an error when the player name is not set", function() {
    const game = {}

    const action = new AddPlayerAction(game);

    (function () {
      action.handle({ });
    }).should.throw("You must provide a 'name' for the new player.");
  });

  it("should throw an error when the player name already exists", function() {
    const game = {
      players: [{
        name: "player1"
      }]
    }

    const action = new AddPlayerAction(game);

    (function () {
      action.handle({ name: "player1" });
    }).should.throw("The player is already registered.");
  });

  it("should convert the action to a JSON", function() {
    const game = {   }

    const action = new AddPlayerAction(game);

    action.toJSON().should.deep.equal({
      by: "*",
      type: "add-player"
    });
  });
});