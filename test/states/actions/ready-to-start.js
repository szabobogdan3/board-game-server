require('chai').should();

const ReadyToStartAction = require('../../../source/states/actions/ready-to-start');

describe("The ready to start action", function () {
  it("should mark the player as ready", function () {
    const game = {
      players: [{
        name: "test"
      }]
    }

    const action = new ReadyToStartAction(game, "test");
    action.handle({ ready: true });

    game.should.deep.equal({
      players: [{
        name: "test",
        ready: true
      }]
    });
  });

  it("should mark only the action player as ready", function () {
    const game = {
      players: [{
        name: "player1"
      }, {
        name: "player2"
      }]
    }

    const action = new ReadyToStartAction(game, "player2");
    action.handle({ ready: true });

    game.should.deep.equal({
      players: [{ name: "player1" }, {
        name: "player2",
        ready: true
      }]
    });
  });

  it("should convert the action to a JSON", function () {
    const game = { players: [{ name: "player1" }] }

    const action = new ReadyToStartAction(game, "player1");

    action.toJSON().should.deep.equal({
      by: "player1",
      type: "ready-to-start"
    });
  });
});