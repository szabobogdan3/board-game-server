require('chai').should();

const BeginGameAction = require('../../../source/states/actions/begin-game');

describe("The begin game action", function () {
  it("should call the beginGame() method", function () {
    let called = false;

    const game = {
      beginGame() {
        called = true;
      }
    }

    const action = new BeginGameAction(game);
    action.handle();

    called.should.equal(true);
  });

  it("should throw an error when the beginGame() method does not exist", function () {
    const game = {}

    const action = new BeginGameAction(game);

    (function () {
      action.handle();
    }).should.throw("The game must have a beginGame() method.");
  });

  it("should convert the action to a JSON", function () {
    const action = new BeginGameAction({});

    action.toJSON().should.deep.equal({
      by: "*",
      type: "begin-game"
    });
  });
});