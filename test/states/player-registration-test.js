require('chai').should();

const PlayerRegistrationState = require('../../source/states/player-registration');

describe("The player registration state", function () {
  it("should wait for the first player to register when the game has no players", function() {
    const game = {
      players: []
    }

    const state = new PlayerRegistrationState(game);
    state.actions().map(a => a.toJSON()).should.deep.equal([{
      by: "*",
      type: "add-player"
    }]);
  });

  it("should allow all registered players to mark themself as ready", function() {
    const game = {
      players: [
        { name: "player1" },
        { name: "player2" }
      ]
    }

    const state = new PlayerRegistrationState(game);
    state.actions().map(a => a.toJSON()).should.deep.equal([{
      by: "*",
      type: "add-player"
    },{
      by: "player1",
      type: "ready-to-start"
    },{
      by: "player2",
      type: "ready-to-start"
    }]);
  });


  it("should allow starting the game when there are two ready players", function() {
    const game = {
      players: [
        { name: "player1", ready: true },
        { name: "player2", ready: true }
      ]
    }

    const state = new PlayerRegistrationState(game);
    state.actions().map(a => a.toJSON()).should.deep.equal([{
      by: "*",
      type: "add-player"
    },{
      by: "*",
      type: "begin-game"
    }]);
  });


  it("should not allow adding players when there are 4 players", function() {
    const game = {
      players: [
        { name: "player1" },
        { name: "player2" },
        { name: "player3" },
        { name: "player4" }
      ]
    }

    const state = new PlayerRegistrationState(game);
    state.actions().map(a => a.toJSON()).should.deep.equal([{
      by: "player1",
      type: "ready-to-start"
    },{
      by: "player2",
      type: "ready-to-start"
    },{
      by: "player3",
      type: "ready-to-start"
    },{
      by: "player4",
      type: "ready-to-start"
    }]);
  });

  it("should not allow adding players when there is one player and the max is a", function() {
    const game = {
      players: [ { name: "player"} ]
    }

    const state = new PlayerRegistrationState(game, 1, 1);
    state.actions().map(a => a.toJSON()).should.deep.equal([{
      by: "player",
      type: "ready-to-start"
    }]);
  });

  it("should throw an error when the min is 0", function() {
    const game = {
      players: [ { name: "player"} ]
    }

    const state = new PlayerRegistrationState(game, 0);

    (function () {
      state.actions();
    }).should.throw('The player registration state must have at least 1 player.');
  });

  it("should throw an error when the max is less than min", function() {
    const game = {
      players: [ { name: "player"} ]
    }

    const state = new PlayerRegistrationState(game, 2, 1);

    (function () {
      state.actions();
    }).should.throw("The player registration state must have the 'max' numbers of players >= than the 'min' one.");
  });
});