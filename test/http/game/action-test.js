require('chai').should();
const { init, destroy, getDb } = require('../setup');

describe('POST /games/:id/:action', function () {
  let server;
  let games;
  let id;

  beforeEach(async function () {
    server = await init();
    games = getDb().collection('games');
    await games.remove({});

    const item = await games.insert({
      type: "space-mine"
    });

    id = item.insertedIds[0].toString();
  });

  afterEach(async function () {
    await destroy();
  });

  it('responds with 200 when the add-player action is triggered', async function () {
    const res = await server.inject({
      method: 'post',
      url: `/games/${id}/add-player`,
      payload: {
        by: "*",
        params: { name: "player1" }
      }
    });

    res.result.should.deep.equal({
      _id: id,
      type: 'space-mine',
      state: 'player-registration',
      actions: [
        { by: '*', type: 'add-player' },
        { by: "player1", type: "ready-to-start" }]
    });
    res.statusCode.should.equal(200);
  });
});
