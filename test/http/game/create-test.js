require('chai').should();
const { init, destroy, getDb } = require('../setup');

describe('POST /games', function () {
  let server;
  let games;

  beforeEach(async function () {
    server = await init();
    games = getDb().collection('games');
    await games.remove({});
  });

  afterEach(async function () {
    await destroy();
  });

  it('responds with 200 when the game exists', async function () {
    const res = await server.inject({
      method: 'post',
      url: '/games',
      payload: {
        game: {
          type: "space-mine"
        }
      }
    });

    res.statusCode.should.equal(200);
    const responseObject = res.result;
    responseObject.game.type.should.equal("space-mine");
    responseObject.game.actions.should.deep.equal([{ by: "*", type: "add-player" }]);

    const newGame = await games.findOne({ _id: responseObject.game._id });
    newGame._id = newGame._id.toString();

    newGame.type.should.equal("space-mine");
    newGame.should.not.have.property("actions");
  });

  it('responds with 400 when the game exists and a state is sent', async function () {
    const res = await server.inject({
      method: 'post',
      url: '/games',
      payload: {
        game: {
          type: "space-mine",
          state: "some-state"
        }
      }
    });

    res.statusCode.should.equal(400);
    res.result.should.deep.equal({
      message: `"game.state" is not allowed`,
      error: "Bad Request",
      statusCode: 400
    });

    const createdGamesCount = await games.count({});
    createdGamesCount.should.equal(0);
  });

  it('responds with 400 when the game does not exist', async function () {
    const res = await server.inject({
      method: 'post',
      url: '/games',
      payload: {
        game: {
          type: "some-undefined-game"
        }
      }
    });

    res.statusCode.should.equal(400);
    res.result.should.deep.equal({
      message: "The 'some-undefined-game' does not exist.",
      error: "Bad Request",
      statusCode: 400
    });

    const createdGamesCount = await games.count({});
    createdGamesCount.should.equal(0);
  });

  it('responds with 400 when the game type is not sent', async function () {
    const res = await server.inject({
      method: 'post',
      url: '/games',
      payload: { game: {} }
    });

    res.statusCode.should.equal(400);
    res.result.should.deep.equal({
      message: '"game.type" is required',
      error: "Bad Request",
      statusCode: 400
    });

    const createdGamesCount = await games.count({});
    createdGamesCount.should.equal(0);
  });
});