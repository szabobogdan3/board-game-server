require('chai').should();
const { init, destroy } = require('./setup');

describe('GET /', function () {
  let server;

  beforeEach(async function () {
    server = await init();
  });

  afterEach(async function () {
    await destroy();
  });

  it('responds with 404', async function () {
    const res = await server.inject({
      method: 'get',
      url: '/'
    });
    res.statusCode.should.equal(404);
  });
});