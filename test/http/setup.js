const Server = require('../../source/server');
const mongodb = require('mongo-mock');
const GameRepository = require('../../source/game/repository');
const SpaceMine = require('../../source/game/space-mine');

let db;
let server;

module.exports.init = async function() {
  mongodb.max_delay = 0;
  const MongoClient = mongodb.MongoClient;
  var url = 'mongodb://localhost:27017/myproject';

  db = await MongoClient.connect(url, {});

  const gameRepo = new GameRepository(db.collection("games"));
  gameRepo.add(SpaceMine);
  server = await Server.init(db, gameRepo);

  return server;
}

module.exports.destroy = async function() {
  if(server) {
    await server.stop();
  }

  if(db) {
    await db.collection("game").remove({});
    await db.close();
  }
}

module.exports.getDb = function() {
  return db;
}

process.on('unhandledRejection', (err) => {
  module.exports.destroy();
});